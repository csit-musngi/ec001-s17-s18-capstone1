package com.zuitt.capstone.services;

import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    public void createUser(User user) {
        userRepository.save(user);
    }


    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public Optional<User> findByUsername(String username){
//        if findByUsername method returns null it will throw a NullPointerException.
//        using .ofNullable method will avoid this from happening.
        return Optional.ofNullable(userRepository.findByUsername(username));
    }
}
