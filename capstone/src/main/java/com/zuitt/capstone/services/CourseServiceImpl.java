package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    public void createCourse(Course course){
        courseRepository.save(course);
    }
    public ResponseEntity deleteCourse(int id){
        courseRepository.deleteById(id);
        return new ResponseEntity<>("Course deleted successfully", HttpStatus.OK);
    }

    public ResponseEntity updateCourse(int id, Course course){
        Course courseForUpdating = courseRepository.findById(id).get();

        courseForUpdating.setName(course.getName());
        courseForUpdating.setDescription(course.getDescription());
        courseForUpdating.setPrice(course.getPrice());

        courseRepository.save(courseForUpdating);
        return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);

    }



    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }
}
