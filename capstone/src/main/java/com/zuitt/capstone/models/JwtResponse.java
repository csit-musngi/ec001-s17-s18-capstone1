package com.zuitt.capstone.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = 416057817890933819L;

    // Properties
    private final String jwttoken;

    // Parametrized Constructor
    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    // Getters and Setters
    public String getToken() {
        return this.jwttoken;
    }
}
